import React, { Component } from 'react';
import axios from 'axios';
import './terminal.css';

class Terminal extends Component {

	constructor() {
		super();

		this.state = {
			cursor: '>',
			cursor_classes: 'cmd-cursor',
			output_span: [],
			cmd_line_span: [], 
			dir:[]
		};

	}

	onFocus = (event) => {
		console.log('class added.')
		this.setState({cursor_classes: 'cmd-cursor-blink'})
	}


	onBlur = (event) => {
		console.log('class removed.')
		this.setState({cursor_classes: 'cmd-cursor'})
	}

	onKeyDown = (event) => {
		let key = event.key;
		let arr = this.state.cmd_line_span;
		

		if(key === 'Enter'){
			let os = this.state.output_span;
			let cmd = arr.join('');
			os.push(cmd);
			this.setState({cmd_line_span: []});

			if(cmd === 'clear'){
				this.setState({output_span: []})
				return;
			}

			if(cmd.split(' ')[0] === 'cd'){
				console.log('cd was called: ',cmd);
			}

			this.setState({output_span: os});

			return;
		}else{
			if(key === 'Backspace'){
				arr.pop();
			}else{
				arr.push(key);
			}
		}

		this.setState({cmd_line_span: arr})
	}

	scroll_to_bottom = () => {
		this.scrollView.scrollIntoView();
	}

	componentDidMount() {
		this.scroll_to_bottom();
	}

	componentDidUpdate() {
		this.scroll_to_bottom();
	}

	render() {
		return (
			<div tabIndex='0' onFocus={this.onFocus} onBlur={this.onBlur} onKeyDown={this.onKeyDown} className='terminal-wrapper p-2 m-2 border rounded'>

				<div className='terminal-output' role='log'>
					<div className='terminal-history'>
						{this.state.output_span.map((item, i) => ( 
							<span className='terminal-history-line' key={i}>
								<span>>&nbsp;</span>
								<span>{item}</span>
							</span>
						))}
					</div>
					<div ref={ref => {this.scrollView = ref;}} className='cmd'>
						<div className='cmd-wrapper'>
							<div className='cmd-prompt'>
								<span>{this.state.cursor}&nbsp;</span>
							</div>
							<div className='cmd-cursor-line'>
									{
										this.state.cmd_line_span.map((item,i) => (
											<span className='cmd_char' key={i}>{item}</span>
										))
									}
								<div className={this.state.cursor_classes}>&nbsp;</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		);
	}
}

export default Terminal;
